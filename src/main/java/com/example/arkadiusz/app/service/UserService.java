package com.example.arkadiusz.app.service;

import com.example.arkadiusz.app.dao.UserDao;
import com.example.arkadiusz.app.dto.UserConverter;
import com.example.arkadiusz.app.dto.UserDto;
import com.example.arkadiusz.app.entities.UserEntity;
import com.example.arkadiusz.app.exception.UserAlreadyExistException;
import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
@Primary
public class UserService implements UserDetailsService
    {
    private UserDao userDao;
    private UserConverter userConverter;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao userDao, UserConverter userConverter, PasswordEncoder passwordEncoder)
        {
        this.userDao = userDao;
        this.userConverter = userConverter;
        this.passwordEncoder = passwordEncoder;
        }

    public Boolean registerUser(UserDto dto)
        {
        UserEntity userEntity = userConverter.convertToEntity(dto);

        validateUser(userEntity);
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));

        userDao.save(userEntity);

        return true;
        }

    public boolean userExist(UserEntity userEntity)
        {
        return userDao.existsUserEntitiesByEmail(userEntity.getEmail());
        }

    public boolean userExist(String userEmail)
        {
        return userDao.existsUserEntitiesByEmail(userEmail);
        }

    public UserEntity getUserByEmail(String email)
        {
        return userDao
                .findUserEntitiesByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("not found"));
        }

    public UserDto getUserByPrincipal()
        {
        if(!(SecurityContextHolder.getContext().getAuthentication() instanceof OAuth2Authentication))
            {
            throw new RuntimeException("Brak tokena Oauth");
            }
        OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        return userConverter.convertToDto(userDao
                .findUserEntitiesByEmail(principal.getName())
                .orElseThrow(() -> new UsernameNotFoundException("not found")));
        }

    private void validateUser(UserEntity userEntity)
        {
        if (userExist(userEntity))
            {
            throw new UserAlreadyExistException();
            }
        }


    public UserEntity getUserById(Long id)
        {
        return userDao.findById(id).get();
        }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
        {
        UserEntity userEntity = getUserByEmail(username);

        UserDetails userDetails = new User(userEntity.getEmail(), userEntity.getPassword(),
                Lists.<GrantedAuthority>newArrayList(new SimpleGrantedAuthority(userEntity.getRole())));

        return userDetails;
        }
    }
