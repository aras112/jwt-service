package com.example.arkadiusz.app.secure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import lombok.Builder;

@Configuration
@EnableAuthorizationServer
public class Server extends AuthorizationServerConfigurerAdapter
    {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsService userDetailsService;

    @Value("${key}")
    private String key;

    @Autowired
    DefaultAccessTokenConverter myAccessTokenConverter;

    @Autowired
    CorsConfiguration corsConfiguration;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception
        {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));

        endpoints.tokenStore(tokenStore())
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .tokenEnhancer(tokenEnhancerChain);
        }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception
        {
        clients.inMemory()
                .withClient("aras")
                .secret("$2a$10$7UL20AdOjXkRC5jT4nHX/OPoHZ2B0a5CQj0I0G6macWf2e4a7OSS2")
                .authorizedGrantTypes("password", "refresh_token")
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(28 * 24 * 3600)
                .scopes("read");
        }

    @Bean
    public TokenStore tokenStore()
        {
        return new JwtTokenStore(accessTokenConverter());
        }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception
        {
        security.addTokenEndpointAuthenticationFilter(new CorsFilter(request -> corsConfiguration));
        super.configure(security);
        }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter()
        {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(key);
        converter.setAccessTokenConverter(myAccessTokenConverter);
        return converter;
        }


    TokenEnhancer tokenEnhancer()
        {
        return new OwnEnhanceToken();
        }
    }
