package com.example.arkadiusz.app.secure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class AuthenticationManagerProvider extends WebSecurityConfigurerAdapter
    {
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
        {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
        }

    @Override
    protected void configure(HttpSecurity http) throws Exception
        {
        http.csrf().disable().cors().disable().authorizeRequests().anyRequest().permitAll();
        }

    @Bean
    @Override
    @Primary
    public AuthenticationManager authenticationManagerBean() throws Exception
        {
        return super.authenticationManagerBean();
        }

    // TODO: 08.05.2019 must add new encoder when endpoint with reg will be ready.
    }
