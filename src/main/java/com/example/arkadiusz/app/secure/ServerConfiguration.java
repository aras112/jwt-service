package com.example.arkadiusz.app.secure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Configuration
public class ServerConfiguration
    {
    @Autowired
    EurekaInstanceConfigBean eurekaInstanceConfigBean;

    @Value("${own.status}")
    private String status;
    @Value("${own.health}")
    private String health;

    @Bean
    public PasswordEncoder passwordEncoder(){
    return new BCryptPasswordEncoder();
    }

    @PostConstruct
    public void init(){
    eurekaInstanceConfigBean.setHealthCheckUrl(health);
    eurekaInstanceConfigBean.setStatusPageUrl(status);
    }
    }
