package com.example.arkadiusz.app.dao;

import com.example.arkadiusz.app.entities.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Long>
    {
    Boolean existsUserEntitiesByEmail(String email);

    Optional<UserEntity> findUserEntitiesById(Long id);
    Optional<UserEntity> findUserEntitiesByEmail(String email);
    }
