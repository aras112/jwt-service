package com.example.arkadiusz.app.dto;

import com.example.arkadiusz.app.entities.UserEntity;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class UserConverter implements Converter<UserEntity, UserDto>
    {
    @Override
    public UserDto convertToDto(UserEntity entity)
        {
        return UserDto.builder()
                .email(entity.getEmail())
                .login(entity.getLogin())
                .name(entity.getName())
                .surname(entity.getSurname())
                .role(entity.getRole())
                .password("blank")
                .yearOfBirth(entity.getYearOfBirth().toString())
                .build();
        }

    @Override
    public UserEntity convertToEntity(UserDto dto)
        {
        return UserEntity.builder()
                .email(dto.getEmail())
                .login(dto.getLogin())
                .name(dto.getName())
                .surname(dto.getSurname())
                .role(dto.getRole())
                .password(dto.getPassword())
                .yearOfBirth(LocalDate.parse(dto.getYearOfBirth()))
                .build();
        }
    }
