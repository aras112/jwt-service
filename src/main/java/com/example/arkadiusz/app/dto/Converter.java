package com.example.arkadiusz.app.dto;

import org.springframework.stereotype.Component;


public interface Converter<ENTITY_TYPE, DTO_TYPE>
    {

    DTO_TYPE convertToDto(ENTITY_TYPE entity);

    ENTITY_TYPE convertToEntity(DTO_TYPE dto);

    }
