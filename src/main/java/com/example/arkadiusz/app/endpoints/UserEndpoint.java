package com.example.arkadiusz.app.endpoints;

import com.example.arkadiusz.app.dao.UserDao;
import com.example.arkadiusz.app.dto.UserDto;
import com.example.arkadiusz.app.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import sun.security.provider.certpath.OCSPResponse;


@RestController("/user")
@CrossOrigin(value = "*" )
public class UserEndpoint
    {

    private UserService userService;

    public UserEndpoint(UserService userService)
        {
        this.userService = userService;
        }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    public void register(@RequestBody UserDto user)
        {
        userService.registerUser(user);
        }

    @GetMapping("/get")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(){
       return userService.getUserByPrincipal();
    }

    }
